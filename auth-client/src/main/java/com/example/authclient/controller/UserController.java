package com.example.authclient.controller;

import com.taocares.security.core.session.SessionManager;
import com.taocares.security.core.session.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private SessionManager sessionManager;

    @GetMapping
    public List<UserInfo> userInfos(){
        List<UserInfo> userInfos = sessionManager.fetchAllLoginUsers();
        UserInfo userInfo = userInfos.get(0);
        for ( HttpSession session : userInfo.getSessions()) {
            session.getAttribute("name");
        }
        return userInfos;
    }

}
