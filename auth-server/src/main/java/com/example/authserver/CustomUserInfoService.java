package com.example.authserver;

import com.taocares.security.oauth.server.provider.UserInfoService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 自定义的OAuth用户信息获取类
 *
 * @author ankang
 * @since 2019/5/23
 */
@Component
public class CustomUserInfoService implements UserInfoService {

    @Override
    public Map<String, Object> loadUserInfo(String username, Set<String> scope) {
        Map<String, Object> result =  new HashMap<>();
        result.put("userId", "1");
        result.put("jobNo", "001");
        result.put("mobile", "18811112222");
        result.put("email", "dummy@taocares.com");
        return result;
    }
}
