package com.example.authserver.login;

import com.taocares.security.core.authentication.handler.AuthenticationDetailsAppender;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CustomAuthenticationDetailsAppender implements AuthenticationDetailsAppender {
    // 此接口中使用的属性要和的UserDetailsHelper中的属性一致
    @Override
    public Map<String, Object> obtainAuthenticationDetails(String username) {
        Map<String, Object> details = new HashMap<>();
        // find by name from DB
        details.put("dept", "研发部");
        details.put("jobNo", 2001L);
        details.put("post", "工程师");
        return details;
    }
}
