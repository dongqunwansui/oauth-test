package com.example.authserver.login;

import com.taocares.security.core.authentication.handler.AbstractOAuth2AwareAuthenticationSuccessHandler;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 只有非OAuth2的请求才会被此类处理
 *
 * @author Ankang
 * @date 2019/1/24
 */
@Component
public class CustomAuthenticationSuccessHandler extends AbstractOAuth2AwareAuthenticationSuccessHandler {
    @Override
    public void doOnAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.sendRedirect("/");
    }
}
